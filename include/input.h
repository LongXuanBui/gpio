#pragma once
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
void ip_init(gpio_num_t gpio_num, gpio_int_type_t intr_type);
void ip_set_callback(void* fn);