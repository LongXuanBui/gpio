
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "output.h"
#include "input.h"
#define BLINK_GPIO 23
#define INPUT_PIN 0
static xQueueHandle gpio_event =NULL;
int state=0;
void ip_call_back(int pin )
{
xQueueSendFromISR(gpio_event,&pin,NULL);
}
void gpio_task_example (void*arg)
{
    int num;
for(;;)
    {
        if(xQueueReceive(gpio_event,&num,portMAX_DELAY))
        {
            printf("hello\n ");
        }
    }
}
void app_main(void)
{
 op_init_gpio(BLINK_GPIO,GPIO_MODE_OUTPUT);
ip_init(INPUT_PIN,GPIO_INTR_ANYEDGE);
ip_set_callback(ip_call_back);
gpio_event = xQueueCreate( 10, sizeof(uint32_t) );
 xTaskCreate(gpio_task_example, "gpio_task_example", 2048, NULL, 10, NULL);
 
}
