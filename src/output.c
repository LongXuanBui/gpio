#include "output.h"
esp_err_t op_init_gpio(gpio_num_t gpio_num, gpio_mode_t mode)
{
    gpio_pad_select_gpio(gpio_num);
    gpio_set_direction(gpio_num, mode);
    return ESP_OK;
}
esp_err_t op_set_level(gpio_num_t gpio_num,uint32_t level)
{
         gpio_set_level(gpio_num, level);
        return ESP_OK;
}